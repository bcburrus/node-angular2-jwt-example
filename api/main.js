var express = require('express');
var cors = require('cors')
var app = express();
var bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/authenticate', function (req, res) {
  
  var email = req.body.email;
  var password = req.body.password;

  if (validateUser(email, password)) {

  var jwt = require('jsonwebtoken');
	var token = jwt.sign({ email: email }, getSecret(), { expiresIn: '5m' });

  res.send(buildResponse(true, token));
}
  else {
    res.send(buildResponse(false, "authentication failed"));
  }
});

app.post('/validateToken', function(req, res) {
	var token = req.body.token;
  var jwt = require('jsonwebtoken');
  try {
    var decoded = jwt.verify(token, getSecret());
    res.send(buildResponse(true, decoded));
  } catch(err) {
    res.send(buildResponse(false, "authentication failed"));
  }
});

app.listen(3000, function() {
    console.log('Listening on port 3000');
})

function getSecret() {
  var fs = require('fs');
  return fs.readFileSync('config/secret.txt', 'utf8');
}

function buildResponse(success, result) {
  if (success) {
    return {status:'success', result:result};
  }
    return {status:'error', message:result};
}

function validateUser(email, password) {
  if (email === 'ben' && password === 'ben') {
    return true;
  }
  return false;
}

var static = require('node-static');
 
var fileServer = new static.Server('../ui');
 
require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        fileServer.serve(request, response);
    }).resume();
}).listen(8080);