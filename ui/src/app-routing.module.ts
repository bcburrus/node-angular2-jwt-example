import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent }   from './login/user.component';
import { HomeComponent }   from './home/home.component';

import { AuthenticationGuard } from './login/authentication.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthenticationGuard] },
  { path: 'login',  component: UserComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}