import { User } from '../_models/user';

export const USERS: User[] = [
	{ id: 1, email: 'ben@ben.com', password: 'pw', name: 'Ben'},
	{ id: 2, email: 'ted@ted.com', password: 'pw', name: 'Ted'},
]