import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { User } from '../_models/user';
import { USERS } from '../_mocks/mock.users';

import 'rxjs/add/observable/throw';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

	constructor (private http: Http) {}
	
	getUsers(): Promise<User[]> {
		return Promise.resolve(USERS);
	}

	getUser(id: number): Promise<User> {
		return this.getUsers().then(users => users.find(user => user.id === id));
	}

	authenticateUser(email: string, password: string) {

		let headers = new Headers({ 'Content-Type': 'application/json'});
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify({ email, password });
		let url = "http://localhost:3000/authenticate";

		this.http.post(url, body, options)
			.subscribe(respnose => {
				var isSuccess = respnose.json().status === 'success' ? true : false;
				if(isSuccess) {
					localStorage.setItem('currentUser', respnose.json().result);
				}
			});
	}

	validateToken(token: string): Promise<boolean> {
		let headers = new Headers({ 'Content-Type': 'application/json'});
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify({ token });
		let url = "http://localhost:3000/validateToken";

		return this.http.post(url, body, options)
			.toPromise()
			.then(response => response.json().status === 'success' ? true : false as boolean);
	}
}