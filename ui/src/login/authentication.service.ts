import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';

import { User } from '../_models/user';

@Injectable()
export class AuthenticationService implements OnInit {

	public token: string;

	ngOnInit(): void {
		var currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.token = currentUser && currentUser.token;
	}
}