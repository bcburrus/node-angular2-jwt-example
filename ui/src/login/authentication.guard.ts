import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UserService } from './user.service';
 
@Injectable()
export class AuthenticationGuard implements CanActivate {
 
    constructor(private router: Router,  
    			private userService: UserService) { }
 
    canActivate() {
        if (localStorage.getItem('currentUser')) {
        	this.userService.validateToken(localStorage.getItem('currentUser'))
        		.then(result => {
        			alert(result);
        			return result;
        		})
        }

        this.router.navigate(['/login']);
        return false;
    }
}