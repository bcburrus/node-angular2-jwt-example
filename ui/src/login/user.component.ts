import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../_models/user';
import { UserService } from './user.service';

@Component({
  moduleId: module.id,
  selector: 'my-users',
  templateUrl: '../../components/login.component.html'
})
export class UserComponent implements OnInit {
  users: User[];

  constructor(
    private router: Router,
    private userService: UserService) { }

  getUsers(): void {
    this.userService.getUsers().then(users => this.users = users);
  }

  ngOnInit(): void {
    this.getUsers();
  }

  authenticateUser(email: string, password: string): void {
    this.userService.authenticateUser(email, password);
  }
}