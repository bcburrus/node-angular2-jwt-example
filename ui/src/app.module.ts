import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { UserComponent } from './login/user.component';
import { HomeComponent } from './home/home.component';

import { UserService } from './login/user.service'

import { AuthenticationGuard } from './login/authentication.guard'

@NgModule({
  imports: [ 
  	BrowserModule,  
  	FormsModule,
    AppRoutingModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [ 
  	AppComponent,
    UserComponent,
    HomeComponent
  ],
  providers: [
    UserService,
    AuthenticationGuard
  ],
  bootstrap: [ 
  	AppComponent 
  ]
})

export class AppModule { }