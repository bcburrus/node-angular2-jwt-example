import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-home',
  template: 'You\'re Already Authenticated'
})
export class HomeComponent {

  constructor(
    private router: Router) { }
}